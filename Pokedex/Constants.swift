//
//  Constants.swift
//  Pokedex
//
//  Created by Juan Ramirez on 9/6/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation

//Everything stored outside of the class are global
let URL_BASE = "http://pokeapi.co"
let URL_POKEMON = "/api/v1/pokemon/"

//http://pokeapi.co/api/v1/pokemon/1

// Closure -- Bloack of code that will be called whenever we want
typealias DownloadComplete = () -> ()
