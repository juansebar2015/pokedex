//
//  Pokemon.swift
//  Pokedex
//
//  Created by Juan Ramirez on 6/5/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Pokemon {
    
    private var _name: String!
    private var _pokedexId: Int!
    private var _description: String!
    private var _type: String!
    private var _defense: String!
    private var _height: String!
    private var _weight: String!
    private var _attack: String!
    private var _nextEvolutionText: String!
    private var _nextEvolutionId: String!
    private var _nextEvolutionLevel: String!
    private var _pokemonURL : String!
    
    private var _evolutions: [String:String]!
    
    //Getters 
    var name: String {
        get {
            if _name == nil {
                _name = ""
            }
            return _name
        }
    }
    
    var pokedexId: Int {
        get {
            if _pokedexId == nil {
                _pokedexId = 0
            }
            return _pokedexId
        }
    }
    
    var description: String {
        get{
            if _description == nil {
                _description = ""
            }
            return _description
        }
    }
    
    var type: String {
        get{
            if _type == nil{
                _type = "nil"
            }
            return _type
        }
    }
    
    var defense: String {
        get{
            if _defense == nil {
                _defense = ""
            }
            return _defense
        }
    }
    
    var height: String {
        get{
            if _height == nil {
                _height = ""
            }
            return _height
        }
    }
    
    var weight: String {
        get{
            if _weight == nil{
                _weight = ""
            }
            return _weight
        }
    }
    
    var attack: String {
        get{
            if _attack == nil {
                _attack = ""
            }
            return _attack
        }
    }
    
    var nextEvolutionText: String {
        get{
            if _nextEvolutionText == nil {
                _nextEvolutionText = ""
            }
            return _nextEvolutionText
        }
    }
    
    var nextEvolutionId: String {
        get{
            if _nextEvolutionId == nil {
                _nextEvolutionId = ""
            }
            return _nextEvolutionId
        }
    }
    
    var nextEvolutionLevel: String {
        get{
            if _nextEvolutionLevel == nil {
                _nextEvolutionLevel = ""
            }
            return _nextEvolutionLevel
        }
    }
    
    init(name: String, pokedexId: Int) {
        self._name = name
        self._pokedexId = pokedexId
        
        self._pokemonURL = "\(URL_BASE)\(URL_POKEMON)\(self._pokedexId)/"
    }
    
    //Download and parse details of Pokemon
    // **** MUST call "completed" after it has finished all processes to continue to were it was called
    func downloadPokemonDetails(completed: DownloadComplete) {
        
        let url = NSURL(string: _pokemonURL)!
        Alamofire.request(.GET, url).responseJSON { response in
            let result = response.result
            
            // Keys of the dictionary are all Strings in this request
            if let dict = result.value as? Dictionary<String, AnyObject> {
                
                if let weight = dict["weight"] as? String {
                    self._weight = weight
                }
                
                if let height = dict["height"] as? String {
                    self._height = height
                }
                
                if let attack = dict["attack"] as? Int {
                    self._attack = "\(attack)"
                }
                
                if let defense = dict["defense"] as? Int {
                    self._defense = "\(defense)"
                }
                
                print(self._weight)
                print(self._height)
                print(self._attack)
                print(self._defense)
                
                if let types = dict["types"] as? [Dictionary<String, String>] where types.count > 0 {
                    
//                    if let name = types[0]["name"]{
//                        self._type = name.capitalizedString
//                    }
//                    
//                    if types.count > 1 {
//                        
//                        for var x = 1 ; x < types.count; x++ {
//                            if let name = types[x]["name"] {
//                                self._type! += "/\(name.capitalizedString)"
//                            }
//                        }
//                    }
                    var count = 0
                    
                    for type in types {
                
                        if count < 1 {  //First value in array
                            self._type = type["name"]!.capitalizedString
                        } else {        //The rest of the values
                            self._type! += "/\(type["name"]!.capitalizedString)"
                        }
                    
                        count += 1
                    }
                } else {
                    self._type = ""
                }
                
                print(self._type)
                
                
                /*
                 * Parse the description data for the Pokemon
                 */
                if let descArr = dict["descriptions"] as? [Dictionary<String,String>] where descArr.count > 0 {
                    
                    if let resourceURL = descArr[0]["resource_uri"] {
                        
                        //print("\(URL_BASE)\(resourceURL)")
                        let descriptionURL = NSURL(string: "\(URL_BASE)\(resourceURL)")!
                        
                        // Make the call to the server
                        Alamofire.request(.GET, descriptionURL).responseJSON{ response in
                            let descResult = response.result
                            
                            if let descDict = descResult.value as? Dictionary<String, AnyObject> {
                                
                                if let desc = descDict["description"] as? String {
                                    self._description = desc
                                    print(self._description)
                                }
                            }
                            
                            // Notifying that the processes have finised and go to closure of where it was called
                            completed()
                        }
                    }
                } else {
                    self._description = ""
                }
                
                //EVOLUTIONS
                if let evolutions = dict["evolutions"] as? [Dictionary<String, AnyObject>] where evolutions.count > 0 {
                    
                    if let evoName = evolutions[0]["to"] as? String {
                        
                        // If "mega" is not in the name then continue
                        // Can't support mega pokemon right now but API has mega data
                        if evoName.rangeOfString("mega") == nil {
                            
                            if let uri = evolutions[0]["resource_uri"] as? String {
                                
                                var pokemonNum = uri.stringByReplacingOccurrencesOfString("\(URL_POKEMON)", withString: "")
                                pokemonNum = pokemonNum.stringByReplacingOccurrencesOfString("/", withString: "")
                                
                                self._nextEvolutionText = evoName
                                self._nextEvolutionId = pokemonNum
                            
                                if let level = evolutions[0]["level"] as? Int {
                                    self._nextEvolutionLevel = "\(level)"
                                }
                                print("\(self._nextEvolutionId) \(self._nextEvolutionText) \(self._nextEvolutionLevel)")
                            }
                        }
                        
                    }
                    
                }
            }
        }
        
    }
    
}
