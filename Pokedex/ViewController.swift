//
//  ViewController.swift
//  Pokedex
//
//  Created by Juan Ramirez on 6/5/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    // Used for sizing, spacing and stuff
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    var pokemon = [Pokemon]()
    var filteredPokemon = [Pokemon]()
    var musicPlayer : AVAudioPlayer!
    var inSearchMode = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        collectionView.delegate = self
        collectionView.dataSource = self
        searchBar.delegate = self
        
        
        // Action it will take when you press the "Enter"/"Return"/"Search" on the keyboard and hides it
        searchBar.returnKeyType = UIReturnKeyType.Done
        
        // Create & Add the Tap gesture recognizer
        //let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.dismissKeyboard))
        //view.addGestureRecognizer(tap)
        
        parsePokemonCSV()
        initAudio()
    }
    
//    func dismissKeyboard(){
//        
//        view.endEditing(true)
//    }
    
    func initAudio() {
        
        let path = NSBundle.mainBundle().pathForResource("music", ofType: "mp3")!
        
        do {
            
            musicPlayer = try AVAudioPlayer(contentsOfURL: NSURL(fileURLWithPath: path))
            musicPlayer.prepareToPlay()
            musicPlayer.numberOfLoops = -1
            musicPlayer.play()
            
        } catch let error as NSError {
            print(error.debugDescription)
        }
        
    }
    
    func parsePokemonCSV() {
        
        let path = NSBundle.mainBundle().pathForResource("pokemon", ofType: "csv")!
        
        do {
            let csv = try CSV(contentsOfURL: path)
            
            let rows = csv.rows
            
            for row in rows {
                let pokeID = Int(row["id"]!)!
                let name = row["identifier"]!
                let poke = Pokemon(name: name, pokedexId: pokeID)
                
                pokemon.append(poke)
            }
            
        } catch let error as NSError{
            print(error.debugDescription)
        }
    }
    
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if inSearchMode {
            return filteredPokemon.count
        }
        
        return pokemon.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCellWithReuseIdentifier("PokeCell", forIndexPath: indexPath) as? PokeCell {
            
            let poke : Pokemon!
            
            if inSearchMode {
                poke = filteredPokemon[indexPath.row]
            } else {
                poke = pokemon[indexPath.row]
            }
            
            cell.configureCell(poke)
            return cell
            
        } else {
            return UICollectionViewCell()
        }
    }
    
    // Used to trigger the segue
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        
        let poke: Pokemon!
        
        if inSearchMode {
            poke = filteredPokemon[indexPath.row]
        } else {
            poke = pokemon[indexPath.row]
        }
        
        print("_______________ \(poke.name)")
        
        performSegueWithIdentifier("PokemonDetailVC", sender: poke)
    }
    
    // To give the size of the grid
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        
        return CGSizeMake(105, 105)
        
    }
 
    @IBAction func musicButtonPressed(sender: UIButton) {
        
        if musicPlayer.playing {
            musicPlayer.stop()
            
            sender.alpha = 0.2
        } else {
            musicPlayer.play()
            
            sender.alpha = 1.0
        }
        
    }
    
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        view.endEditing(true)
    }
    
    
    // Shows the Pokemon filtering by what the user types. Enters whenever text did change entering
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {

        // If the user deletes all of the text in search bar
        if searchBar.text == nil || searchBar.text == "" {
            inSearchMode = false
            
            view.endEditing(true)      //Hides the keyboard
            collectionView.reloadData()
        } else {
            inSearchMode = true
        
            let lower = searchBar.text!.lowercaseString
            
            /* 
             * Method of arrays used to filter by some constraint
             * $0 -> Grabs one object and checks if it has the string occurrence in it
             * If there is a match then it will add it to the filteredPokemon array
             * "filter" method is very efficient, better than going through every single entry in for loop
             */
            filteredPokemon = pokemon.filter({$0.name.rangeOfString(lower) != nil})
            
            // Refresh the table view
            collectionView.reloadData()
        }
        
        
    }
    
    // Set up sender objects before performing the segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "PokemonDetailVC" {
            
            if let detailsVC = segue.destinationViewController as? PokemonDetailVC {
                
                if let poke = sender as? Pokemon {
                    detailsVC.pokemon = poke
                }
            }
        }
        
    }
}

