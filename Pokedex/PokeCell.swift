//
//  PokeCell.swift
//  Pokedex
//
//  Created by Juan Ramirez on 6/5/16.
//  Copyright © 2016 Juan Ramirez. All rights reserved.
//

import UIKit

class PokeCell: UICollectionViewCell {
   
    @IBOutlet weak var thumbImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    var pokemon: Pokemon!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
    }
    
    func configureCell(pokemon: Pokemon) {
        
        self.pokemon = pokemon
        
        nameLabel.text = self.pokemon.name.capitalizedString
        thumbImage.image = UIImage(named: "\(self.pokemon.pokedexId)")
    }
    
}
